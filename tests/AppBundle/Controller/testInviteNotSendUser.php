<?php
/**
 * Created by PhpStorm.
 * User: venpan
 * Date: 04/03/2018
 * Time: 14:40
 */

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
class testInviteNotSendUser extends WebTestCase
{
    /**
     * test for invitation not sent/cancelled
     * same for decline
     */
    public function testInviteNotSendUser()
    {
        $client = static::createClient([
            'base_uri' => 'php7dev/jamAPI/web/app_dev.php/'
        ]);

        $response = $client->request(
            'POST',
            '/invite/send/2',
            [   'to' => '4',
                'status' => '0' | '1',
            ]
        );
        $this->assertEquals(200, $response->getStatusCode());
    }
}