<?php


namespace Tests\AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class testUserNotFoundAction extends WebTestCase
{
    /**
     * Test for getUserAction not found
     */
    public function testUserNotFoundAction()
    {
        $client = static::createClient([
            'base_uri' => 'php7dev/jamAPI/web/app_dev.php/'
        ]);

        $response = $client->request('GET', '/user/20');

        $this->assertEquals(404, $response->getStatusCode());
    }

}