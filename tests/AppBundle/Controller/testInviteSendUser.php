<?php
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class testInviteSendUser extends WebTestCase
{
    /**
     * test for invitation sent / accept
     */
    public function testInviteSendUser()
    {
        $client = static::createClient([
            'base_uri' => 'php7dev/jamAPI/web/app_dev.php/'
        ]);

        $response = $client->request('POST', '/invite/send/2',
            [
                'to' => '4',
                'status' => '1',
            ]);

        $this->assertEquals(200, $response->getStatusCode());
    }
}