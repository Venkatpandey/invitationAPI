<?php
namespace Tests\AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class testUseraction extends WebTestCase
{
    /**
     * Test for getUserAction found
     */
    public function testUserFoundAction()
    {
        $client = static::createClient([
            'base_uri' => 'php7dev/jamAPI/web/app_dev.php/'
        ]);

        $response = $client->request('GET', '/user/2');

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('location', $data);
        $this->assertEquals('beta', $data['password']);
    }
}