<?php
/**
 * Created by PhpStorm.
 * User: venpan
 * Date: 03/03/2018
 * Time: 08:12
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Invite;
use AppBundle\Entity\UserPair;
use AppBundle\Model\UserModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\User;


class InviteController extends FOSRestController
{
    /**
     * Responsible to retrive user profile and status of recived/sent invitation
     *
     * @Route("/user/{id}")
     * @param $id
     * @return \FOS\RestBundle\View\View|object
     */
    public function getUserAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $UserInfo = new UserModel();
        if ($singleresult !== null) {
            // find invitation sent
            $query = $em->createQuery(
                'SELECT u FROM AppBundle\Entity\Invite u WHERE u.fromUser = ' . $id .'AND u.status = 1');
            $sent = $query->getResult();

            // find invitation recived
            $query = $em->createQuery(
                'SELECT u FROM AppBundle\Entity\Invite u WHERE u.toUser = ' . $id .'AND u.status = 1');
            $recived = $query->getResult();

            // Load data to usermodel object
            $UserInfo->setName($singleresult->getName());
            $UserInfo->setLocation($singleresult->getLocation());
            $UserInfo->setRecievedInvites(count($recived));
            $UserInfo->setSentInvites(count($sent));
        } else {
            return new View("User not found", Response::HTTP_NOT_FOUND);
        }

        return $UserInfo;
    }

    /**
     * Responsible to handle invitation related calls
     *
     * @Route("/invite/{subaction}/{from}")
     * @param                                           $subaction (send | accdec)
     * @param                                           $from
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postInviteUserAction($subaction, $from, Request $request)
    {
        $request->request->all();
        $to = $request->get('to');
        $status = $request->get('status');

        return $this->{$subaction."Action"}($from, $to, $status);
    }

    /**
     * Responsible for sending an invitation
     *
     * @param $from
     * @param $to
     * @param $status
     * @return \FOS\RestBundle\View\View
     */
    private function sendAction($from, $to, $status)
    {
        $Invite = new Invite();
        if (empty($from) || empty($to)) {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $Invite->setFromUser($from);
        $Invite->setToUser($to);
        $Invite->setStatus($status);
        $em = $this->getDoctrine()->getManager();
        $em->persist($Invite);
        $em->flush();

        return new View("Invite Sent Successfully", Response::HTTP_OK);
    }

    /**
     * Responsible for accepting or declining of invitation, works both ways
     *
     * @param $from
     * @param $to
     * @param $status
     * @return \FOS\RestBundle\View\View
     */
    private function accdecAction ($from, $to, $status)
    {
        if ($status === '1') {
            // this is accept
            $UserPair = new UserPair();
            $UserPair->setSender($from);
            $UserPair->setInvited($to);
            $UserPair->setSince(date('Y-m-d H:i:s'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($UserPair);
            $em->flush();

            // mark this invite request as done
            $this->markAsDone([
                $from,
                $to,
                0,
            ]);

            return new View("Invite accepted Successfully", Response::HTTP_OK);
        } elseif ($status === '0') {
            // mark as cancelled, done
            $this->markAsDone([
                $from,
                $to,
                $status,
            ]);
            return new View("Invite declined Successfully", Response::HTTP_OK);
        }
    }

    /**
     * Marks invite as false, can be deleted if needed
     *
     * @param array $final
     */
    private function markAsDone(array $final)
    {
        $EntityManager = $this->getDoctrine()->getEntityManager();
        $QueryBuilder = $EntityManager->createQueryBuilder();
        $Query = $QueryBuilder->update('AppBundle\Entity\Invite', 'u')
            ->set('u.status', $QueryBuilder->expr()->literal($final[2]))
            ->where('u.fromUser = ?1 AND u.toUser = ?2')
            ->setParameter(1, $final[0])
            ->setParameter(2, $final[1])
            ->getQuery();

        $Result = $Query->execute();
    }
}