<?php
/**
 * Created by PhpStorm.
 * User: venpan
 * Date: 03/03/2018
 * Time: 13:12
 */

namespace AppBundle\Model;


use AppBundle\Entity\User;

class UserModel extends User
{
    private $recievedInvites;

    private $sentInvites;

    /**
     * @return mixed
     */
    public function getRecievedInvites()
    {
        return $this->recievedInvites;
    }

    /**
     * @param mixed $recievedInvites
     */
    public function setRecievedInvites($recievedInvites)
    {
        $this->recievedInvites = $recievedInvites;
    }

    /**
     * @return mixed
     */
    public function getSentInvites()
    {
        return $this->sentInvites;
    }

    /**
     * @param mixed $sentInvites
     */
    public function setSentInvites($sentInvites)
    {
        $this->sentInvites = $sentInvites;
    }


}