<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserPair
 *
 * @ORM\Table(name="user_pair")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserPairRepository")
 */
class UserPair
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sender", type="string", length=255)
     */
    private $sender;

    /**
     * @var string
     *
     * @ORM\Column(name="invited", type="string", length=255)
     */
    private $invited;

    /**
     * @var string
     *
     * @ORM\Column(name="since", type="string", length=255)
     */
    private $since;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sender
     *
     * @param string $sender
     *
     * @return UserPair
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set invited
     *
     * @param string $invited
     *
     * @return UserPair
     */
    public function setInvited($invited)
    {
        $this->invited = $invited;

        return $this;
    }

    /**
     * Get invited
     *
     * @return string
     */
    public function getInvited()
    {
        return $this->invited;
    }

    /**
     * Set since
     *
     * @param string $since
     *
     * @return UserPair
     */
    public function setSince($since)
    {
        $this->since = $since;

        return $this;
    }

    /**
     * Get since
     *
     * @return string
     */
    public function getSince()
    {
        return $this->since;
    }
}

